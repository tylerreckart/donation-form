import React, { useState, useEffect } from 'react';
// Components.
import Banner from '../Banner';
import DonationForm from '../DonateModule';
import Loader from '../Loader';
// Styles.
import styles from './app.module.scss';

const App = () => {
  const [data, setData] = useState({});
  const [date, setDate] = useState(false);
  const [isLoading, setIsLoading] = useState(true);

  // Mount an effect that fetches donation metrics from the API on mount.
  useEffect(() => {
    if (isLoading) {
      fetch('/api/metrics')
        .then((res) => res.json())
        .then(({ result }) => {
          // Update the data state object.
          setData(result);
          // Set the reference end date.
          setDate(Date.parse(result.endDate));
          // Date has been received, finish loading.
          setIsLoading(false);
        });
    }
  }, []);

  /**
   * A helper function that sends a GET request to the API that causes all
   * donation metrics to be reset to their starting state.
   */
  function handleReset() {
    fetch('/api/reset')
      .then((res) => res.json())
      .then(({ result }) => {
        // Update the data state object.
        setData(result);
      });
  }

  /**
   * A helper function that updates the API any time a new date is selected.
   * @param {string} newDate - The date to be set.
   */
  function handleDateChange(newDate) {
    fetch('/api/endDate', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ newDate }),
    })
      .then((res) => res.json())
      .then(({ result }) => {
        // Update the date state value.
        setDate(Date.parse(newDate));
        // Update the data state object.
        setData(result);
      });
  }

  if (isLoading) {
    return (
      <div className={styles.loadingContainer}>
        <Loader />
      </div>
    );
  }

  return (
    <>
      <Banner
        handleReset={handleReset}
        startDate={date}
        handleDateChange={handleDateChange}
      />
      <div className={styles.appWrapper}>
        <DonationForm data={data} />
      </div>
    </>
  );
};

export default App;
