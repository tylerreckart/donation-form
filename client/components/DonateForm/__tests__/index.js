import React from 'react';
import { render } from '@testing-library/react';
import DonateForm from '../index';

describe('The <DonateForm /> component', () => {
  it('should render with a valid structure', () => {
    const { container } = render(
      <DonateForm
        handleSubmit={() => {}}
        error={false}
        success={false}
      />
    );

    expect(container).toMatchSnapshot();
  });

  it('should render with an error message if one exists', () => {
    const { container } = render(
      <DonateForm
        handleSubmit={() => {}}
        error={'There was an error with your donation. Please try again.'}
        success={false}
      />
    );

    expect(container).toMatchSnapshot();
  });

  it('should render with a success message if one exists', () => {
    const { container } = render(
      <DonateForm
        handleSubmit={() => {}}
        error={false}
        success={'Your donation has been received. Thank you!'}
      />
    );

    expect(container).toMatchSnapshot();
  });
});
