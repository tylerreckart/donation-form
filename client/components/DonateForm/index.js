import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Form, { Input } from '../Form';
// Styles.
import styles from './donateForm.module.scss';

const DonationForm = ({ handleSubmit, error, success }) => {
  const [value, setValue] = useState('');

  function handleChange(event) {
    setValue(event.target.value);
  }

  function onSubmit(event) {
    event.preventDefault();

    // Submit the donation.
    handleSubmit(value);
    // Clear the input.
    setValue('');
  }

  return (
    <Form className={styles.form} onSubmit={onSubmit}>
      <div className={styles.formElementsWrapper}>
        <Input
          id="donation-form__input"
          className={styles.input}
          type="text"
          label="$"
          aria-label="Enter a donation amount"
          onChange={handleChange}
          value={value}
          aria-errormessage="donation-error"
        />
        <input
          id="donation-form__submit-button"
          className={styles.submitButton}
          type="submit"
          value="Give Now"
        />
      </div>

      {error !== false ? (
        <span
          id="donation-error"
          className={styles.errorMessage}
        >
          {error}
        </span>
      ) : null}

      {success !== false ? (
        <div
          id="success-message"
          className={styles.successMessage}
          role="status"
        >
          {success}
        </div>
      ) : null}
    </Form>
  );
};

DonationForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  error: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]).isRequired,
  success: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]).isRequired,
};

export default DonationForm;
