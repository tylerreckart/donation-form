import React from 'react';
import PropTypes from 'prop-types';
// Styles.
import styles from './progressBar.module.scss';

const ProgressBar = ({ goal, currentAmount }) => (
  <div className={styles.progressBarWrapper}>
    <div
      className={styles.progressBar}
      style={{
        width: (() => {
          if (currentAmount === 0) {
            return '0%';
          }

          return currentAmount >= goal ? '100%' : `${(currentAmount / goal) * 100}%`;
        })(),
        borderRadius: currentAmount >= goal ? '4px 4px 0 0' : '4px 0 0 0',
      }}
    />
  </div>
);

ProgressBar.propTypes = {
  goal: PropTypes.number.isRequired,
  currentAmount: PropTypes.number.isRequired,
};

export default ProgressBar;
