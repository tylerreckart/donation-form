import React from "react";
import { render } from "@testing-library/react";
import ProgressBar from "../index";

describe("The <ProgressBar /> component", () => {
  test("Should look like this when currentAmount < goal", () => {
    const { container } = render(
      <ProgressBar goal={5000} currentAmount={1250} />
    );

    expect(container.firstChild).toMatchInlineSnapshot(`
      <div
        class="progressBarWrapper"
      >
        <div
          class="progressBar"
          style="width: 25%; border-radius: 4px 0 0 0;"
        />
      </div>
    `);
  });

  test("Should look like this when currentAmount > goal", () => {
    const { container } = render(
      <ProgressBar goal={5000} currentAmount={5500} />
    );

    expect(container.firstChild).toMatchInlineSnapshot(`
      <div
        class="progressBarWrapper"
      >
        <div
          class="progressBar"
          style="width: 100%; border-radius: 4px 4px 0 0;"
        />
      </div>
    `);
  });
});
