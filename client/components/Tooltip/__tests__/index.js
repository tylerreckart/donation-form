import React from 'react';
import { render } from '@testing-library/react';
import Tooltip from '../index';

describe('The <Tooltip /> component', () => {
  test('Should look like this when currentAmount < goal', () => {
    const { container } = render(
      <Tooltip goal={5000} currentAmount={1250} />
    );

    expect(container).toMatchSnapshot();
  });

  test('Should look like this when currentAmount > goal', () => {
    const { container } = render(
      <Tooltip goal={5000} currentAmount={5500} />
    );

    expect(container).toMatchSnapshot();
  });
});