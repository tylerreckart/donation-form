import React from 'react';
import PropTypes from 'prop-types';
import { motion } from 'framer-motion';
import styles from './tooltip.module.scss';

const Tooltip = ({ goal, currentAmount }) => {
  const motionVariants = {
    visible: {
      opacity: 1,
      top: '-55px',
    },
    hidden: {
      opacity: 0,
      top: 0,
    },
  };

  function renderContent() {
    if (currentAmount < goal) {
      return (
        <div className={styles.tooltipContent}>
          <sup className={styles.dollarSign}>$</sup>
          <span className={styles.amount}>{goal - currentAmount}</span>
          {' still needed to fund this project'}
        </div>
      );
    }

    return (
      <div className={styles.tooltipContent}>
        <sup className={styles.dollarSign}>$</sup>
        <span className={styles.amount}>{currentAmount}</span>
        {' of '}
        <sup className={styles.dollarSign}>$</sup>
        <span className={styles.amount}>{goal}</span>
        {' raised thanks to our supporters'}
      </div>
    );
  }

  return (
    <motion.div
      initial="hidden"
      animate="visible"
      variants={motionVariants}
      transition={{ duration: 0.5 }}
      className={styles.tooltipContainer}
      role="tooltip"
    >
      {renderContent()}
    </motion.div>
  );
};

Tooltip.propTypes = {
  goal: PropTypes.number.isRequired,
  currentAmount: PropTypes.number.isRequired,
};

export default Tooltip;
