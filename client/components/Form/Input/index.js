import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
// Styles.
import styles from './input.module.scss';

export default function Input(props) {
  const {
    className,
    id,
    label,
  } = props;
  return (
    <div className={styles.inputContainer}>
      <label htmlFor={id} className={styles.label}>
        {label}
      </label>
      <input
        {...props}
        name={id}
        className={classNames(
          styles.input,
          className,
        )}
      />
    </div>
  );
}

Input.defaultProps = {
  type: 'text',
  placeholder: '',
  className: '',
};

Input.propTypes = {
  className: PropTypes.string,
  id: PropTypes.string.isRequired,
  type: PropTypes.string,
  label: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};
