import React from 'react';
import { render } from '@testing-library/react';
import Form, { Input } from '../index';

describe('The <Form /> component', () => {
  it('should render with a valid structure', () => {
    const { container } = render(
      <Form onSubmit={() => {}}>
        <Input
          id="input"
          className="input"
          type="text"
          label="my testable input"
          onChange={() => {}}
          value="input value"
        />
        <input type="submit" value="Submit Form" />
      </Form>
    );

    expect(container).toMatchSnapshot();
  });
});
