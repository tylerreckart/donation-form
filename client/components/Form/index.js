import React from 'react';
import PropTypes from 'prop-types';
import Input from './Input';

export { Input };

const Form = ({ className, children, onSubmit }) => (
  <form className={className} onSubmit={onSubmit}>
    {children}
  </form>
);

Form.defaultProps = {
  className: '',
  onSubmit: () => {},
};

Form.propTypes = {
  className: PropTypes.string,
  children: PropTypes.arrayOf(PropTypes.element).isRequired,
  onSubmit: PropTypes.func,
};

export default Form;
