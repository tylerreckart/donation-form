import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { formatHeading, formatCTA } from './utils';
import DonateForm from '../DonateForm';
import ProgressBar from '../ProgressBar';
import Tooltip from '../Tooltip';
// Styles.
import styles from './donateModule.module.scss';

const DonationModule = ({ data }) => {
  const {
    goal, currentAmount, donationsCount, daysRemaining,
  } = data;

  const [error, setError] = useState(false);
  const [success, setSuccess] = useState(false);
  const [donorCount, setDonorCount] = useState(donationsCount);
  const [currentCount, setCurrentCount] = useState(currentAmount);

  // Mount an effect that updates the donor count and current donation amount
  // any time the data prop changes. If there are any error or success messages,
  // clear them at this time too.
  useEffect(() => {
    setDonorCount(donationsCount);
    setCurrentCount(currentAmount);

    if (success) {
      setSuccess(false);
    }

    if (error) {
      setError(false);
    }
  }, [data]);

  /**
   * A function that takes a number and submits the donation amount to the API.
   * @param {number} value - The donation amount to be submitted.
   */
  function submitForm(value) {
    const submitButton = document.getElementById('donation-form__submit-button');

    // If the submit button exists and is the active element, clear focus on submit.
    if (submitButton && document.activeElement === submitButton) {
      submitButton.blur();
    }

    fetch('/api/donate', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ amount: value }),
    })
      .then((res) => res.json())
      .then(({ status }) => {
        if (status === 'success') {
          // Update the donor count in the current state context to avoid extra API requests.
          setDonorCount(donorCount + 1);
          // Update the current donation amount.
          setCurrentCount(currentCount + value);
          // Display a success message.
          setSuccess('Your donation has been received. Thank you!');
        } else {
          // Display an error message.
          setError('There was an error with your donation. Please try again.');
        }
      });
  }

  function handleSubmit(value) {
    // If an error message is present on submit, clear it.
    if (error) {
      setError(false);
    }
    // If a success message is present on submit, clear it.
    if (success) {
      setSuccess(false);
    }
    // Convert the submitted value to a number.
    const asInt = Number(value);
    // If the converted value is NaN, display an error message and bail early.
    if (Number.isNaN(asInt)) {
      setError('Donations must be a number. Please try again');
      return;
    }
    // If the value is less than 5, display an error message and bail early.
    if (asInt < 5) {
      setError('Donations must be at least $5. Please try again');
      return;
    }
    // Checks passed, submit the form.
    submitForm(asInt);
  }

  return (
    <div className={styles.moduleWrapper} role="main">
      <Tooltip goal={goal} currentAmount={currentCount} />

      <div className={styles.formContainer}>
        <ProgressBar goal={goal} currentAmount={currentCount} />

        <h1 className={styles.heading}>
          {formatHeading(daysRemaining, daysRemaining === false)}
        </h1>
        <p className={styles.cta}>
          {formatCTA(donorCount, daysRemaining === false)}
        </p>

        {daysRemaining !== false && (
          <DonateForm
            handleSubmit={handleSubmit}
            error={error}
            success={success}
          />
        )}
      </div>
    </div>
  );
};

DonationModule.propTypes = {
  data: PropTypes.shape({
    goal: PropTypes.number.isRequired,
    currentAmount: PropTypes.number,
    donationsCount: PropTypes.number,
    daysRemaining: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.bool,
    ]),
  }).isRequired,
};

export default DonationModule;
