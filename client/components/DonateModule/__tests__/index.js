import React from 'react';
import { render } from '@testing-library/react';
import DonateModule from '../index';

describe('The <DonateModule /> component', () => {
  it('Should render with a valid structure', () => {
    const data = {
      goal: 5000,
      currentAmount: 1250,
      donationsCount: 4,
      endDate: '2020-10-23T00:00:00.000Z',
      daysRemaining: 'eight',
    };

    const { container } = render(
      <DonateModule data={data} />
    );

    expect(container).toMatchSnapshot();
  });
});
