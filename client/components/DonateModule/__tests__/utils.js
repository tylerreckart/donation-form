import { render } from '@testing-library/react';
const { formatHeading, formatCTA } = require('../utils');

describe('DonateModule utility functions', () => {
  describe('formatHeading', () => {
    test('Should return "time is up" string if isClosed is true', () => {
      expect(formatHeading('', true))
        .toEqual('Time is up to fund this project');
    });

    test('Should return last day string if daysRemaining is zero', () => {
      expect(formatHeading('zero', false))
        .toEqual('Today is the last day to fund this project');
    });

    test('Should return with correct verbiage when daysRemaining is one', () => {
      expect(formatHeading('one', false))
        .toEqual('Only one day left to fund this project');
    });

    test('Should return with correct verbiage when daysRemaining is greater than one', () => {
      expect(formatHeading('two', false))
        .toEqual('Only two days left to fund this project');
    });
  });

  describe('formatCTA', () => {
    test('Should return thank you string if isClosed is true', () => {
      expect(formatCTA(0, true))
        .toEqual('Thank you to all of the supporters who donated to this project');
    });

    test('Should return generic CTA if donor count is zero', () => {
      expect(formatCTA(0, false))
        .toEqual('Donote now and support this project');
    });

    test('Should return whith correct verbiage when donor count is one', () => {
      const { container } = render(formatCTA(1, false));

      expect(container).toMatchInlineSnapshot(`
        <div>
          Join the 
          <strong>
            1
          </strong>
           other donor who has
           already supported this project
        </div>
      `);
    });

    test('Should return with correct verbiage when donor count is greater than one', () => {
      const { container } = render(formatCTA(2, false));

      expect(container).toMatchInlineSnapshot(`
        <div>
          Join the 
          <strong>
            2
          </strong>
           other donors who have
           already supported this project
        </div>
      `);
    });
  });
});