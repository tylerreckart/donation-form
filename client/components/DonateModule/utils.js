import React from 'react';

/**
 * A helper function that formats the heading for display.
 * @param {string} daysRemaining - The number of days remaining in the campaign.
 * @param {boolean} isClosed - Is the current date past the campaign end date?
 * @returns {string} The formatted heading
 */
export function formatHeading(daysRemaining, isClosed) {
  if (isClosed) {
    return 'Time is up to fund this project';
  }

  if (daysRemaining === 'zero') {
    return 'Today is the last day to fund this project';
  }

  const verbiage = daysRemaining !== 'one' ? 'days' : 'day';

  return `Only ${daysRemaining} ${verbiage} left to fund this project`;
}

/**
 * A helper function that formats the call to action based on donor count.
 * @param {number} count - The number of donors.
 * @param {boolean} isClosed - Is the current date past the campaign end date?
 * @returns {string} The formatted call to action.
 */
export function formatCTA(count, isClosed) {
  // Campaign has ended, return thank you string.
  if (isClosed) {
    return 'Thank you to all of the supporters who donated to this project';
  }

  // If there are no donations, return a generic call to action.
  if (count === 0) {
    return 'Donote now and support this project';
  }

  // Determine the correct verbiage to use based on donation plurality.
  const isPlural = count > 1;

  return (
    <>
      {'Join the '}
      <strong>{count}</strong>
      {` other ${isPlural ? 'donors' : 'donor'} who ${isPlural ? 'have' : 'has'}`}
      {' already supported this project'}
    </>
  );
}
