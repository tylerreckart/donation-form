import React from 'react';
import PropTypes from 'prop-types';
// Components.
import DatePicker from 'react-datepicker';
import CustomDatePicker from './datePicker';
// Styles.
import styles from './banner.module.scss';
import 'react-datepicker/dist/react-datepicker.css';

const Banner = ({ handleReset, startDate, handleDateChange }) => (
  <div className={styles.banner} role="banner">
    <div className={styles.bannerContent}>
      <p>
        This project is connected to an Express server that allows you to change
        the date and reset the donations to test edge cases.
      </p>
      <div className={styles.controls}>
        <DatePicker
          selected={startDate}
          onChange={handleDateChange}
          customInput={<CustomDatePicker />}
          withPortal
        />
        <button
          className={styles.resetButton}
          type="button"
          onClick={handleReset}
        >
          Reset Donations
        </button>
      </div>
    </div>
  </div>
);

Banner.propTypes = {
  handleReset: PropTypes.func.isRequired,
  startDate: PropTypes.number.isRequired,
  handleDateChange: PropTypes.func.isRequired,
};

export default Banner;
