import React from 'react';
import PropTypes from 'prop-types';
// Styles.
import styles from './banner.module.scss';

const CustomDatePicker = ({ value, onClick }) => (
  <div>
    <span className={styles.dateLabel}>Choose a date</span>
    <button
      aria-label="Choose a date"
      className={styles.datePicker}
      type="button"
      onClick={onClick}
    >
      {value}
    </button>
  </div>
);

CustomDatePicker.defaultProps = {
  value: '',
  onClick: () => {},
};

CustomDatePicker.propTypes = {
  value: PropTypes.string,
  onClick: PropTypes.func,
};

export default CustomDatePicker;
