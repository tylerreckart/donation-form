import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
// Global application styles.
import './client.module.scss';

if (module.hot) {
  module.hot.accept();
}

ReactDOM.render(
  <App message="Hello, World!" />,
  document.getElementById('root'),
);
