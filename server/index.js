const path = require('path');
const express = require('express');
const cors = require('cors');
const app = express();
const { toWords } = require('number-to-words');
const MongoClient = require('mongodb').MongoClient;
require('dotenv').config();

app.use(cors());
app.use(express.json());

app.use(express.static(path.resolve(__dirname, '../build')));
app.get('/', (req, res) => {
  res.sendFile(path.resolve(__dirname, '../build', 'index.html'));
});

/**
 * A helper function that formats the `daysRemaining` string for display on
 * the front-end. If the current date is past the end date, return false.
 * @param {string} endDate - The end date to compare against.
 * @returns {string|boolean} Days remaining.
 */
function getDaysRemaining(endDate) {
  const currentTime = new Date().getTime();
  const endTime = new Date(endDate).getTime();
  const difference = (endTime - currentTime) / (1000 * 3600 * 24);
  
  if (difference < 0) {
    return false;
  }

  return toWords(difference);
}

MongoClient.connect(process.env.MONGO_URL, { useUnifiedTopology: true })
  .then(async (client) => {
    const db = await client.db('donate-form');
    
    /**
     * A route that returns donation metrics.
     */
    app.get('/api/metrics', async (req, res) => {
      const metrics = await db.collection('metrics').findOne({ goal: { $eq: 5000 } });
      const donations = await db.collection('donations').find({}).toArray();
      
      if (metrics && donations) {
        const { goal, currentAmount, endDate } = metrics;
        const donationsCount = donations.length;
  
        res.send({
          status: 'success',
          result: {
            goal,
            currentAmount,
            donationsCount,
            endDate,
            daysRemaining: getDaysRemaining(endDate),
          }
        });
      } else {
        res.send({
          status: 'failed',
          message: 'there was an error fetching metrics',
        });
      }
    });
    
    /**
     * A route that updates the endDate value and returns updated donation metrics.
     */
    app.post('/api/endDate', async (req, res) => {
      const metrics = await db.collection('metrics').findOne({ goal: { $eq: 5000 } });
      const donations = await db.collection('donations').find({}).toArray();
      const { newDate } = req.body;
      
      if (metrics) {
        const { _id } = metrics;
        
        const update = await db.collection('metrics').updateOne(
          { _id },
          { $set: { endDate: newDate } }
        );
        
        if (update.result) {
          res.send({
            status: 'success',
            result: {
              ...metrics,
              daysRemaining: getDaysRemaining(newDate),
              donationsCount: donations.length || 0,
            },
          });
        } else {
          res.send({
            status: 'failed',
            message: 'there was an error updating the end date'
          });
        }
      }
    });
    
    /**
     * A route that resets donation metrics to their default values.
     */
    app.get('/api/reset', async (req, res) => {
      const metrics = await db.collection('metrics').findOne({ goal: { $eq: 5000 } });
      
      if (metrics) {
        const { _id, goal, endDate } = metrics;
        
        const update = await db.collection('metrics').updateOne(
          { _id },
          { $set: { currentAmount: 0 } }
        );
        const remove = await db.collection('donations').deleteMany({});
        
        if (update.result && remove.result) {
          res.send({
            status: 'success',
            result: {
              goal,
              currentAmount: 0,
              donationsCount: 0,
              daysRemaining: getDaysRemaining(endDate),
            },
          });
        } else {
          res.send({
            status: 'failed',
            message: 'there was an error resetting the database'
          });
        }
      }
    });
    
    /**
     * A route that updates the database with each donation and increments the total count.
     */
    app.post('/api/donate', async (req, res) => {
      const metrics = await db.collection('metrics').findOne({ goal: { $eq: 5000 } });
      const { _id, currentAmount } = metrics;
      const { amount } = req.body;
      
      const insert = await db.collection('donations').insertOne({ amount });
      const update = await db.collection('metrics').updateOne(
        { _id },
        { $set: { currentAmount: currentAmount + amount } }
      );
      
      if (insert.result && update.result) {
        res.send({ status: 'success' });  
      } else {
        res.send({
          status: 'failed',
          message: 'there was an error submitting the donation'
        });
      }
    });
  })
  .catch((error) => {
    console.error('error');
  });

app.listen(process.env.PORT, () =>
  console.log(`Express server listening on port ${process.env.PORT}`)
);
