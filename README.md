# ActiveCampaign Donate Form Exercise  
This repository serves as the codebase for the completed coding exercise outlined in the Front End Coding Challenge document.

## Acceptance Criteria  
The acceptance criteria for the coding challenge were as follows:  

* Build a donation form with a goal of raising $5000 
* Donations cannot be less than $5  
* Total donation dollars must be updated with each donation  
* The campaign progress bar must be updated with each donation 
* The number of donors must be updated with each donation  

## Approach
Once I had the AC's outlined for the project. I started by identifying edge cases in the outline in order to account for them in the development of the app. The edge cases I identified were:  

* No verbiage was outlined if time ran out in the campaign  
* No verbiage was outlined if only a single day remained in the campaign  
* No verbiage was outlined if there were no donations  
* No verbiage was outlined for a single donor  
* No verbiage was outlined for when the campaign surpassed its goal  
* No verbiage was outlined for error/success messaging  

These edge cases were easy enough to account for in this exercise, however, in a production context there should be coordination with marketing/design on how the messaging for each edge case should be presented.

In order to account for those edge cases in a way that is easily testable, I wrote a small Express server that serves as a local API that can track donations, the total amount donated, and the end date in order to make the UI more dynamic.

## Local Development  
In order to develop locally, you'll also have to set a few environment variables:
```
NODE_ENV=development
PORT=8000
MONGO_URL=mongodb+srv://<USERNAME>:<PASSWORD>@<HOST_URL>/<DB_NAME>
```
Then you can run the following commands:
```
npm install
npm run dev:server
npm run dev:web
```

To run a production build, you'll have to remove the `development` flag in the `.env` configuration and run the following command:
```
npm run build
```
To serve the production build on the port set in the `.env` configuration, run the following command:
```
npm run start
```

## Testing  
React components and utility functions are tested with Jest. In order to run the test suite locally, run the following command:
```
npm run test
```
