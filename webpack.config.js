const path = require('path');
const webpack = require('webpack');
const openBrowser = require("react-dev-utils/openBrowser");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const sass = require('node-sass');
const sassUtils = require('node-sass-utils')(sass);
const siteTheme = require(path.resolve(__dirname, 'client/config/theme.js'));
// .env configuration.
require('dotenv').config({
  path: path.resolve(__dirname, '.env')
});

module.exports = {
  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name].bundle.css',
      chunkFilename: '[id].css',
    }),
    new webpack.HotModuleReplacementPlugin(),
  ],
  entry: path.resolve(__dirname, 'client', 'index.js'),
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'bundle.js',
  },
  devServer: {
    compress: true,
    clientLogLevel: 'silent',
    contentBase: './build',
    publicPath: '/build/',
    port: 8080,
    hot: true,
    after: () => {
      openBrowser('http://localhost:8080');
    },
    proxy: {
      '/api': {
        target: 'htto://localhost:8000',
        secure: false,
      },
    },
  },
  module: {
    rules: [
      {
        test: /\.(jsx|js)$/,
        include: path.resolve(__dirname, 'client'),
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: [
                ['@babel/preset-env', {
                  "targets": "defaults",
                }],
                '@babel/preset-react',
              ],
            },
          },
          {
            loader: 'eslint-loader',
            options: {
              fix: true,
            },
          },
        ],
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.module\.s(a|c)ss$/i,
        include: path.resolve(__dirname, 'client'),
        exclude: /node_modules/,
        use: [
          process.env.NODE_ENV === 'development'
          ? 'style-loader'
          : {
            loader: MiniCssExtractPlugin.loader,
            options: {
              hmr: process.env.NODE_ENV === 'development',
            },
          },
          {
            loader: 'css-loader',
            options: {
              modules: true,
              sourceMap: process.env.NODE_ENV === 'development',
            },
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: process.env.NODE_ENV === 'development',
              sassOptions: {
                functions: {
                  "siteTheme($keys)": function(keys) {
                    keys = keys.getValue().split(".");
                    let result = siteTheme;
                    let i;
                    for (i = 0; i < keys.length; i++) {
                      result = result[keys[i]];
                    }
                    result = sassUtils.castToSass(result);
                    return result;
                  },
                },
              },
            },
          },
        ],
      },
    ],
  },
  resolve: {
    extensions: ['.js', '.jsx', '.scss'],
  },
};


